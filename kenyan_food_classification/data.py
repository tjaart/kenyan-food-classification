import os
from os.path import exists

import pandas as pd
import pytorch_lightning as L
import torch
from PIL import Image
from torch.utils.data import DataLoader, Dataset, random_split
from torchvision import transforms

from .classes import Food


class KenyanFoodDataset(Dataset):
    def __init__(self, data_root: str, transform=None, stage: str = "train"):
        self.data_root = data_root
        self.transform = transform
        self.stage = stage

        input_file = os.path.join(self.data_root, "train.csv")
        if stage == "predict":
            input_file = os.path.join(self.data_root, "test.csv")

        self.csv = pd.read_csv(os.path.join(self.data_root, input_file))

    def __len__(self):
        return len(self.csv)

    def __getitem__(self, idx):
        path = os.path.join(
            self.data_root, "images", "images", f"{self.csv['id'].get(idx, -1)}.jpg"
        )
        if not exists(path):
            print(f"Path: {path} does not exist!")
            return

        image = Image.open(path)

        if self.transform:
            image = self.transform(image)

        # For prediction data, we only return image tensors
        if self.stage == "predict":
            return image

        # For training data, we return image tensors + class number
        if self.stage == "train":
            class_value = self.csv["class"].get(idx, None)
            target = -1 if not class_value else Food[class_value].value
            # target = one_hot_classes[target_index]

            return image, target


class KenyanFoodDataModule(L.LightningDataModule):
    def __init__(
        self, data_root: str = "data", batch_size: int = 32, num_workers: int = 2, model_transforms = transforms.Compose([])
    ):
        super().__init__()
        self.data_root = data_root
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.transforms = model_transforms

        self.aug_transforms = transforms.Compose(
            [
                transforms.RandomAdjustSharpness(sharpness_factor=2),
                transforms.RandomEqualize(),
                transforms.RandomHorizontalFlip(),
                transforms.RandomPerspective(),
                transforms.GaussianBlur(kernel_size=3),
                transforms.RandomAutocontrast(),
                # transforms.RandomVerticalFlip(),
                self.transforms
            ]
        )

    def setup(self, stage=None):
        if stage == "fit" or stage is None:
            # Global seed has already been set
            generator = torch.Generator()

            full_dataset = KenyanFoodDataset(
                self.data_root, transform=self.aug_transforms, stage="train"
            )

            image_count = len(full_dataset)

            # Randomly split the dataset into 80% training and 20% validation
            validation_count = int(image_count * (20 / image_count))
            train_count = image_count - validation_count

            self.train, self.val = random_split(
                full_dataset, [train_count, validation_count], generator=generator
            )

        if stage == "predict" or stage is None:
            self.predict = KenyanFoodDataset(
                self.data_root, transform=self.transforms, stage="predict"
            )

    def train_dataloader(self):
        return DataLoader(
            self.train,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
        )

    def predict_dataloader(self):
        return DataLoader(
            self.predict,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
        )


