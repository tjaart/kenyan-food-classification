import torchvision
from pytorch_lightning.callbacks import Callback
from torchmetrics import MeanMetric
from torchmetrics.classification import (
    MulticlassAccuracy,
    MulticlassF1Score,
    MulticlassPrecision,
    MulticlassRecall,
)
from torchvision import transforms


class LoggerCallbacks(Callback):
    def on_train_start(self, trainer, pl_module):
        pl_module.train_acc = MulticlassAccuracy(pl_module.hparams.num_classes).to(pl_module.device)
        pl_module.train_loss = MeanMetric().to(pl_module.device)
        pl_module.train_prec = MulticlassPrecision(
            pl_module.hparams.num_classes).to(pl_module.device)
        pl_module.train_recall = MulticlassRecall(
            pl_module.hparams.num_classes).to(pl_module.device)
        pl_module.train_f1 = MulticlassF1Score(pl_module.hparams.num_classes).to(pl_module.device)

    def on_train_batch_end(self, trainer, pl_module, outputs, batch, batch_idx):
        _, target = batch
        pred = outputs["pred"]
        loss = outputs["loss"]

        acc = pl_module.train_acc(pred, target)
        pl_module.train_loss(loss)
        pl_module.train_prec(pred, target)
        pl_module.train_recall(pred, target)
        pl_module.train_f1(pred, target)

        # log batch metrics to a logger
        pl_module.log_dict({"train/batch_acc": acc,
                            "train/batch_loss": loss})

    def on_train_epoch_start(self, trainer, pl_module):
        # super().on_train_epoch_start(trainer, pl_module)

        pl_module.train_acc.reset()
        pl_module.train_loss.reset()
        pl_module.train_prec.reset()
        pl_module.train_recall.reset()
        pl_module.train_f1.reset()

    def on_train_epoch_end(self, trainer, pl_module):
        # super().on_train_epoch_end(trainer, pl_module)

        avg_train_loss = pl_module.train_loss.compute()
        avg_train_acc = pl_module.train_acc.compute()
        avg_train_prec = pl_module.train_prec.compute()
        avg_train_recall = pl_module.train_recall.compute()
        avg_train_f1 = pl_module.train_f1.compute()

        pl_module.log_dict({"step": pl_module.current_epoch,
                            "train/loss": avg_train_loss,
                            "train/acc": avg_train_acc,
                            "train/precision": avg_train_prec,
                            "train/recall": avg_train_recall,
                            "train/f1": avg_train_f1,
                            }, logger=True)

    def on_validation_start(self, trainer, pl_module):
        pl_module.valid_acc = MulticlassAccuracy(pl_module.hparams.num_classes).to(pl_module.device)
        pl_module.valid_loss = MeanMetric().to(pl_module.device)
        pl_module.valid_prec = MulticlassPrecision(
            pl_module.hparams.num_classes).to(pl_module.device)
        pl_module.valid_recall = MulticlassRecall(
            pl_module.hparams.num_classes).to(pl_module.device)
        pl_module.valid_f1 = MulticlassF1Score(pl_module.hparams.num_classes).to(pl_module.device)

    def on_validation_batch_end(self, trainer, pl_module, outputs, batch, batch_idx):
        data, target = batch
        pred = outputs["pred"]
        loss = outputs["loss"]

        # # Using Torchmetrics Module API
        acc = pl_module.valid_acc(pred, target)  # performs accuracy calculation and tracking
        pl_module.valid_loss(loss)  # performs tracking of loss
        pl_module.valid_prec(pred, target)
        pl_module.valid_recall(pred, target)
        pl_module.valid_f1(pred, target)

        denormalize = transforms.Compose([
            transforms.Normalize((0, 0, 0), (1/0.229, 1/0.224, 1/0.225)),
            transforms.Normalize((-0.485, -0.456, -0.406), (1, 1, 1)),
            # transforms.ToPILImage()
        ])

        sample_imgs = []
        for img in data[:6]:
            sample_imgs.append(denormalize(img))

        grid = torchvision.utils.make_grid(sample_imgs)
        pl_module.logger.experiment.add_image('example_images', grid, 0)
        
        pl_module.log_dict({"valid/batch_acc": acc,
                            "valid/batch_loss": loss})

    def on_validation_epoch_start(self, trainer, pl_module):
        # super().on_validation_epoch_start(trainer, pl_module)

        pl_module.valid_acc.reset()
        pl_module.valid_loss.reset()
        pl_module.valid_prec.reset()
        pl_module.valid_recall.reset()
        pl_module.valid_f1.reset()

    def on_validation_epoch_end(self, trainer, pl_module):
        # super().on_validation_epoch_end(trainer, pl_module)

        avg_val_loss = pl_module.valid_loss.compute()
        avg_val_acc = pl_module.valid_acc.compute()
        avg_val_prec = pl_module.valid_prec.compute()
        avg_val_recall = pl_module.valid_recall.compute()
        avg_val_f1 = pl_module.valid_f1.compute()

        pl_module.log_dict({"step": pl_module.current_epoch,
                            "valid/acc": avg_val_acc,
                            "valid/loss": avg_val_loss,
                            "valid/precision": avg_val_prec,
                            "valid/recall": avg_val_recall,
                            "valid/f1": avg_val_f1,
                            }, logger=True)



