from enum import Enum


class Food(Enum):
    bhaji = 0
    chapati = 1
    githeri = 2
    kachumbari = 3
    kukuchoma = 4
    mandazi = 5
    masalachips = 6
    matoke = 7
    mukimo = 8
    nyamachoma = 9
    pilau = 10
    sukumawiki = 11
    ugali = 12

