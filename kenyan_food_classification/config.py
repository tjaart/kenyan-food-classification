import os
from dataclasses import dataclass

from torchvision import models


@dataclass(frozen=True)
class DataConfiguration:
    if(os.environ.get("KAGGLE_RUNTIME")):
        data_root: str = "/kaggle/input/opencv-pytorch-classification-project-2/"
        num_workers: int = 4
    else:
        data_root: str = "."
        num_workers: int = 7
    batch_size: int = 16
    num_classes = 13


@dataclass(frozen=True)
class TrainingConfiguration:
    epochs_count: int = 100
    # I use the lighting LearningRateFinder callback, but set an initial value here anyway
    init_learning_rate: float = 0.0001
    log_interval: int = 5
    test_interval: int = 1
    weights = models.ResNet152_Weights.IMAGENET1K_V2
    # weights = models.MobileNet_V3_Large_Weights.IMAGENET1K_V1
    # precision: str = "16-mixed"


