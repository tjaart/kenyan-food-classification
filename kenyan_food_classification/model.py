import pytorch_lightning as L
import torch
import torch.nn.functional as F
from torchvision import models

from .config import TrainingConfiguration

train_config = TrainingConfiguration()



class KenyanFoodClassifier(L.LightningModule):
    def __init__(self, learning_rate, num_classes):
        super().__init__()

        self.save_hyperparameters()

        # Use the pretrained model and weights
        self.model = models.resnet152(weights=train_config.weights)
        # Set the number of output classes
        last_layer_in = self.model.fc.in_features
        self.model.fc = torch.nn.Linear(last_layer_in, self.hparams.num_classes)

        # # Replace the last layer with number of classes
        # self.model = models.vgg16(weights=self.hparams.weights)
        # self.model.classifier[6] = torch.nn.Linear(in_features=4096, out_features=self.hparams.num_classes)

        # self.model = models.mobilenet_v3_large(weights=train_config.weights)
        # self.model.classifier[3] = torch.nn.Linear(in_features=1280, out_features=self.hparams.num_classes)

    # I add the **_, here and below to fix annoying warning messages:
    # "Method 'xxx' overrides "LightingModule" in an incomatible manner"
    def forward(self, *args, **_):
        x = args[0]
        return self.model(x)

    def training_step(self, batch, batch_idx):
        data, target = batch

        output = self(data)

        prob = F.softmax(output, dim=1)
        pred = torch.argmax(prob, dim=1)

        loss = F.cross_entropy(output, target)

        return {"loss": loss, "pred": pred}

    def validation_step(self, batch, batch_idx):
        data, target = batch

        output = self(data)

        prob = F.softmax(output, dim=1)
        pred = torch.argmax(prob, dim=1)

        loss = F.cross_entropy(output, target)

        return {"loss": loss, "pred": pred}

    # def predict_step(self, batch, batch_idx):
    #     return self(batch)

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.model.parameters(), lr=self.hparams.learning_rate, momentum=0.9)
        # optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.hparams.learning_rate)
        lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer)

        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": lr_scheduler,
                "monitor": "valid/loss",
                "frequency": train_config.log_interval
            }
        }

