import pytorch_lightning as L

from .config import DataConfiguration, TrainingConfiguration
from .data import KenyanFoodDataModule
from .logger_callback import LoggerCallbacks
from .model import KenyanFoodClassifier

train_config = TrainingConfiguration()
data_config = DataConfiguration()

def training_validation():
    # random seed
    L.seed_everything(21, workers=True)

    # init model
    model = KenyanFoodClassifier(learning_rate=train_config.init_learning_rate, num_classes=data_config.num_classes)

    # init the data module
    data_module = KenyanFoodDataModule(
        data_root=data_config.data_root,
        batch_size=data_config.batch_size,
        num_workers=data_config.num_workers,
        model_transforms=train_config.weights.transforms()
    )

    model_checkpoint = L.pytorch_lightning.callbacks.ModelCheckpoint(
        monitor="valid/acc",
        mode="max",
        auto_insert_metric_name=False,
        save_weights_only=True,
    )

    trainer = L.Trainer(
        accelerator="auto",
        deterministic=True,
        devices="auto",
        # logger=[L.loggers.TensorBoardLogger(save_dir=os.getcwd(), version=1, name="lightning_logs")], DVCLiveLogger(log_model=True)],
        strategy="auto",
        max_epochs=train_config.epochs_count,
        callbacks=[
            LoggerCallbacks(),
            # L.pytorch_lightning.callbacks.LearningRateFinder(),
            # L.pytorch_lightning.callbacks.EarlyStopping(
            #     "valid/loss", mode="min", patience=10, strict=False, verbose=True),
            # L.pytorch_lightning.callbacks.StochasticWeightAveraging(swa_lrs=1e-2),
            model_checkpoint,
        ],
    )

    # tuner = L.tuner.Tuner(trainer)
    # tuner.lr_find(model, datamodule=data_module)

    trainer.fit(model, datamodule=data_module)

    return model, data_module, model_checkpoint
