
import torch

from .submission import create_submission
from .validation import training_validation

torch.set_float32_matmul_precision('medium')

def main():
    model, data_module, model_checkpoint = training_validation()

    create_submission(model, data_module, model_checkpoint)
