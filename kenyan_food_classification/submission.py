import os
import random

import pandas as pd
import torch
import torch.nn.functional as F

from .classes import Food
from .config import DataConfiguration, TrainingConfiguration
from .model import KenyanFoodClassifier

data_config = DataConfiguration()
train_config = TrainingConfiguration()


def create_submission(_model, data_module, model_checkpoint):
    random.seed(21)

    my_model = KenyanFoodClassifier.load_from_checkpoint(
        model_checkpoint.best_model_path
    )

    test_csv = pd.read_csv(os.path.join(data_config.data_root, "test.csv"))

    my_model.freeze()
    my_model.eval()

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    my_model.to(device)

    predictions = []
    data_module.setup(stage="predict")
    predict_dataloader = data_module.predict_dataloader()

    i = 0
    for batch in predict_dataloader:
        with torch.no_grad():
            y_hat = my_model(batch.to(device))

            prob = F.softmax(y_hat, dim=1)
            pred = torch.argmax(prob, dim=1)

            for j in range(len(pred)):
                predictions.append([test_csv["id"][i], Food(pred[j].item()).name])
                i += 1

    output_file = pd.DataFrame(predictions, columns=["id", "class"])
    output_file.to_csv("submission.csv", index=False)
