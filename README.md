# Kenyan Food Classification

This is my solution to the [OpenCV Kaggle competition](https://kaggle.com/competitions/opencv-pytorch-classification-project-2)

# Usage with Kaggle

The [Kaggle API CLI client](https://github.com/Kaggle/kaggle-api) needs to be installed to use this method.

## Update the code

For this script to successfully run on Kaggle, we upload the `kenyan_food_classification` package as a dataset.

To run, using your own kaggle account, you will need to update `kenyan_food_classification/dataset-metadata.json`.

```bash
# Create the dataset:
kaggle datasets create -p kenyan_food_classification

#Update the dataset:
kaggle datasets version -p kenyan_food_classification -m "Update"
```

## Run the kernel

To run, using your own kaggle account, you will need to update `kernel-metadata.json`.

```bash
# Push changes to Kaggle:
kaggle kernels push

# Download the output from your last run:
kaggle kernels status kernel_name
# where kernel_name will be in the form kaggle_username/kaggle_project_name
```
## Update code when


# Run locally

All the needed dependencies are available in the Kaggle kernel, this is only needed to run the code locally.

## Installation
`Python3` and `pip` is required. (Kaggle kernels use Python 3.10 at the time of writing)

Dependencies can be installed using the following command:
```bash
# Install for production
pip install .
# Install for development
pip install --editable .
```


## Data

The data can be downloaded [from here](https://www.kaggle.com/competitions/opencv-pytorch-classification-project-2/data)
