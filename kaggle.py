import os
import sys
from shutil import copytree

copytree(src = "/kaggle/input/kenyan-food-classification", dst = "/kaggle/working/kenyan_food_classification")

sys.path.insert(0, "/kaggle/working")

os.environ["KAGGLE_RUNTIME"] = "1"

from kenyan_food_classification.main import main

main()
